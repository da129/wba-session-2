# Cheat Sheet - Datatypes

* **string**:
  ```python
    #strings are always inside matching double or single quotes
    name = "Oakley"
    breed = 'Corgi'

    # we can use string interpolation to pass variables into strings
    sentance = "My dogs name is {} and he is a {}.".format(name, breed)

    # or the more modern (post python 3.6) f string
    # use this when possible, it is faster and it reads better :) 
    sentance = f"My dogs name is {name} and he is a {corgi}" 
  ```

* **integers and floats**:
  ```python
    # integers are whole numbers while floats are decimals
    x = 2
    y = 2.0 


    # you can use most common math operators without any imports
    2 + 2   # => 4  (add)
    5 - 3   # => 2  (subtract)
    4 * 5   # => 20 (multiply)
    10/2    # => 5  (divide)
    2**3    # => 8  (exponent)
    10 % 3  # => 1  (remainder)
  ```


* **boolean and comparison operators**:
  ```python
    # math operators will return True or False
    2 > 1     # => True
    5 < 20    # => True
    19 >= 19  # => True
    17 >= 87  # => False
    5 == 2    # => False

    # "and" comparison, BOTH sides of the "and" must be "truthy" 
    # note: returns the last object that completes the condition, or the first to break it)
    True and True   # => True
    True and False  # => False
    17 and "Oakley" # => "Oakley"
    None and True   # => None

    # "or" comparison, ONE side of the "or" must be "truthy" 
    # note: returns the last object that breaks the condition, or the first to complete it)
    True or True   # => True
    True or False  # => True
    17 or "Oakley" # => 17
    None or True   # => True
    None or False  # => False
  ```
   

* **list**:
  ```python 
    # define a list using square brackets [] 
    vowels = ['a','o','e','u']

    # access a list usind a number index, start counting with zero
    vowels[0] # => 'a'
    vowels[2] # => 'e'

    # add an object to a list by using the append method
    vowels.append('i') 
    vowels # => ['a','o','e','u', 'i']
  ```

* **dictionary**:
  ```python
    # define a dictionary useing curly brackets {} 
    plane = {"name": "Caravan", "engine": "turboprop", "high_wing": True}

    # access a dictionary using the "key" of the value
    plane["engine"] # => "turboprop"

    # add a new object by defining a new key and value
    plane["manufacturer"] = "Cessna"
    plane => # {"name": "Caravan", "engine": "turboprop", "high_wing": True, "manufacturer": "Cessna"}
  ```

* **loop**:
  ```python 
    # the while loop will run as long an the condition remains "truthy", so be sure to have a getaway car!
    # note the indentation
    count = 0
    while count < 3:
      print(f"the count is {count}")
      count += 1 #adding 1 to count so the loop eventually ends

    # => the count is 0
    # => the count is 1
    # => the count is 2 

    # the for loop iterates over an "iterable" object
    for number in range(3):
      print(f"the number is {number}")
    # => the number is 0
    # => the number is 1
    # => the number is 2 

    # or more commen than range, an array
    basket = ["bread", "milk", "oranges"]
    for item in basket:
      print(f"You have {item} in the basket.")
    # => You have bread in the basket.
    # => You have milk in the basket.
    # => You have oranges in the basket.
  ```

* **conditional**:
  ```python 
    # a conditionl will execute, if the statement it is evaluating returns something "truthy"
    # note the indentation
    if(3>5):
      print("3 is Greater than 5!")
    elif(5<10):
      print("5 is less than 10)
    else:
      print("This will print if all elifs ind the if statment fail to run.")
  ```
