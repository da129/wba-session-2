class Hand:
  def __init__(self, name='Default', cards=[]):
    self.name = name
    self.cards = cards
    self.total = 0
    self.busted = False
  
  def add_cards(self, cards):
    if type(cards) is list:
      self.cards += cards
    else:
      self.cards.append(cards)

  #TODO use this to calculate hard vs soft hand
  def calculate_ace(self):
      self.total += 1 if self.total > 10 else 11

  def show_hand(self):
    print("Showing hand for", self.name)
    for card in self.cards:
      print(card.name)

  def total_hand(self):
    self.total = 0
    aces = []
    for card in self.cards:
      value = card.value
      if(value == "K" or value == "Q" or value == "J"):
        self.total += 10
      elif(value == 'A'): # deal with aces later
        aces.append(value)
      else:
        self.total += int(value)

    #now calculate any aces
    if aces:
      for ace in aces:
        self.calculate_ace() 
    if(self.total > 21): self.busted = True