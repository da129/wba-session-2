from card import Card
from deck import Deck
from hand import Hand
import game_rules as rules
import pdb

#setup the game
deck = Deck()
user_hand = Hand(name="Danai", cards=[deck.draw(), deck.draw()])

face_up_card = deck.draw()
face_down_card = deck.draw()
dealer_hand = Hand(name="Dealer", cards=[face_up_card, face_down_card])
print ('### BLACKJACK ###')


### if dealer showing A, check for game over
rules.is_game_over(face_up_card=face_up_card, face_down_card=face_down_card)

player_phase = True

while player_phase and not user_hand.busted:
  print("\n")
  #show the user hand
  user_hand.show_hand()
  user_hand.total_hand()
  print("User hand totals", user_hand.total)

  # #show the dealer hand
  print("Dealer is showing", face_up_card.value)

  print("==================")
  choice = rules.player_choice()
  if choice == 'draw':
    user_hand.add_cards(deck.draw())
    user_hand.total_hand()
  else:
    player_phase = False


dealer_hand.total_hand()
while dealer_hand.total < 17:
  print(f"############ DEALER HAND {dealer_hand.total}")
  print("\n")
  #show the user hand
  user_hand.show_hand()
  user_hand.total_hand()
  print("User hand totals", user_hand.total)

  # #show the dealer hand
  dealer_hand.show_hand()
  dealer_hand.total_hand()
  print("Dealer hand totals", dealer_hand.total)

  print("==================")
  dealer_hand.add_cards(deck.draw())
  dealer_hand.total_hand()


rules.game_over(user_hand, dealer_hand)