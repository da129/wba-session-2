import sys 

#check if dealer has pocket 21
def dealer_auto_win(face_down_card, face_up):
  if face_up == "A":
    if face_down_card.value == "K" or face_down_card.value == "Q" or face_down_card.value == "J"  or face_down_card.value == "10":
      print("The Dealer has 21")
      sys.exit()

  if face_up == "10":
    if face_down_card.value == "A":
      print("The Dealer has 21")
      sys.exit()

def is_game_over(dealer_hand=False, player_hand=False, face_up_card=False, face_down_card=False):
  if face_up_card and face_up_card.value == "A":
    dealer_auto_win(face_down_card, "A")
  elif face_up_card and face_down_card.value == "K" or face_down_card.value == "Q" or face_down_card.value == "J"  or face_down_card.value == "10":
    dealer_auto_win(face_down_card, "10")

def game_over(user_hand, dealer_hand):
  print('\n')
  #show the user hand
  user_hand.show_hand()
  user_hand.total_hand()
  print("User hand totals", user_hand.total)
  print("\n")
  # #show the dealer hand
  dealer_hand.show_hand()
  dealer_hand.total_hand()
  print("Dealer hand totals", dealer_hand.total)
  print("==================")

  if user_hand.total > dealer_hand.total and user_hand.total < 22:
    print("The Player has won!")
  if dealer_hand.total > user_hand.total and dealer_hand.total < 22:
    print("The Dealer has won!")
  if dealer_hand.total == user_hand.total:
    print("Push")
  if user_hand.total > 21:
    print("Player Busts, Dealer Wins")
  if dealer_hand.total > 21:
    print("Dealer Busts, Player Wins")
  sys.exit()

def player_choice():
  print("What is your move?")
  print("* (h)it")
  print("* (s)tand")
  choice = input("Enter you choice: ")

  if choice == "h":
    return "draw"
  elif choice == 's': 
    return "stand"
  else:
    print("INVALID MOVE")
    return player_choice()




