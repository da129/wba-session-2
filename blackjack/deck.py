from card import Card
import random

class Deck:
  def __init__(self, cards=[], count=1):
    self.count = count
    self.cards = cards
    self.create_deck()


  def cards_remaining(self):
    return len(self.cards)


  def create_deck(self):
    suits = ["♣️ Clubs ♣️", "♥️ Hearts ♥️", "♠️ Spades ♠️", "♦️ Diamonds ♦️"]
    values = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

    for suit in suits:
      for value in values:
        self.cards.append(Card(value, "{} of {}".format(value, suit)))
    random.shuffle(self.cards)
  

  def draw(self, qty=1):
    if qty > 1:
      cards = []
      for i in range(qty):
        cards.append(self.cards.pop(1))
      return cards
    else:
      return self.cards.pop(1)


  def print_deck(self):
    for card in self.cards:
      print(card.name)
    print("{} cards in the deck.".format(self.cards_remaining()))
  
