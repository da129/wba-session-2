class Card:

  def __init__(self, *, suit, value, color, face_value):
    self.suit = suit
    self.value = value
    self.face_value = face_value
    self.color = color

  def __repr__(self):
    return f"{self.face_value} of {self.suit}"
