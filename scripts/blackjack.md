# Blackjack

## Description
The goal is to create a watered down terminal version of the game Blackjack. You can read more on the rules here if you are unfamiliar with the game https://bicyclecards.com/how-to-play/blackjack/

In our version, you simply need to create a 1 on 1 situation: player vs dealer. You should use a standard 52 card deck for your game.



### Features
* recommended you use 3 classes: Card, Deck, Hand (not required)
* Game is between 1 player, and 1 dealer
* Player has 2 options: Draw Card, or stand.
* Dealer must draw to all totals of 16 or less and stand on all totals of 17 to 21
* If Dealer show card is a "10" value card' dealer should check for an "A"(auto win if A)
* You not have to worry about Dealer "soft" 16s.
* you do not need to worry about betting

### Bonus
* Extend the program to allow betting on a game.
* Add options to double down and split the hand
* Add the ablitiy for mulitple players
* Add in mulitple decks (casinos use 6-8)
