# User Input Tracker

## Description

Create a program which will take repeated input from the user and generate statistics on those inputs.


Your goal is to create a program to keep track of user input. You will repeatedly ask the user to enter a number, and as long as the user types a number and hits enter, you will ask again. If the user hits enter on a blank line (without typing a number first), this means that the user is finished.

Once the user is finished, you should present statistics about the inputs.  You should give:

* The total of all the numbers.
* The average of all the numbers.

Your program should still work if the user inputs negative numbers and/or non-integer rational numbers (e.g. `3.45`).

If the user enters something that is not a number (e.g. `Potato`), your program should state that this was a mistake and ask again.


## Bonus

Extend your program to accept strings as well.  If the user starts off by entering a string (e.g. `correct`), then the program will accept strings for subsequent inputs.  Rather than showing statistics at the end, it will show the concatenated version of the inputs.  For example, if the inputs are the following:

* `correct`
* `horse`
* `battery`
* `staple`

Then the output will be `correcthorsebatterystaple`.

Your program should continue to work exactly like Normal Mode if the user starts by entering a number.  If the user mixes numbers and strings in the same run, your program should tell the user to start over, then stop execution.

