from card import Card
import random

class Deck:
  def __init__(self):
    self.cards = []
    self.build()

  def assign_color(self, suit):
    if suit == 'clubs' or suit == 'spades':
      return 'black'
    else:
      return 'red'

  def build(self):
    suits = ['clubs', 'spades', 'diamonds', 'hearts']
    face_values = ['2','3','4','5','6','7','8','9','10','J','Q','K','A']
    values ={'2': 2,'3': 3,'4': 4,'5': 5,'6': 6,'7': 7,'8': 8,'9': 9,'10': 10,'J': 10,'Q': 10,'K': 10,'A': 11}

    for suit in suits:
      for value in face_values:
        card = Card(suit=suit, face_value=value, value=values[value], color=self.assign_color(suit))
        self.cards.append(card)
    random.shuffle(self.cards)

  def draw(self):
    return self.cards.pop(1)
