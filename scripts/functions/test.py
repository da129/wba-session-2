import unittest
import mock
from my_functions import fizzbuzz
from my_functions import multiply


class TestOurMethods(unittest.TestCase):

  # Test guess evaluation
  def test_multiply_ones(self):
    self.assertEqual( multiply(10,1), 10 )
    self.assertEqual( multiply(1,1), 1 )
    self.assertEqual( multiply(1,22), 22 )

  def test_multiply_zeros(self):
    self.assertEqual( multiply(10,0), 0 )
    self.assertEqual( multiply(0,1), 0 )
    self.assertEqual( multiply(0,0), 0 )

  def test_multiply_others(self):
    self.assertEqual( multiply(10,10), 100 )
    self.assertEqual( multiply(7,6), 42 )
    self.assertEqual( multiply(6,7), 42 )


  #####
  # Test fizzbuzz
  #####
  def test_one(self):
    self.assertEqual(1, fizzbuzz(1)) 

  def test_three(self):
    self.assertEqual("Fizz", fizzbuzz(3)) 

  def test_five(self):
    self.assertEqual("Buzz", fizzbuzz(5)) 

  def test_eight(self):
    self.assertEqual(8, fizzbuzz(8)) 

  def test_ten(self):
    self.assertEqual("Buzz", fizzbuzz(10)) 

  def test_fifteen(self):
    self.assertEqual("FizzBuzz", fizzbuzz(15)) 

  def test_twenty_three(self):
    self.assertEqual(23, fizzbuzz(23)) 

  def test_thirty(self):
    self.assertEqual("FizzBuzz", fizzbuzz(30)) 

if __name__ == '__main__':
    unittest.main()