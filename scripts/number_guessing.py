import random


def check_guess(guess, guesses, answer):
  if guess == answer:
    return True
  elif guess in guesses:
    print("You are high, but in a different way.")
  elif guess < answer: 
    print("You are low.")
  elif guess > answer:
    print("You are high.")


def generate_number():
  return random.randint(1,100)


def get_user_guess():
  return input("Guess a number between 1-100: ")


def end_game(game_win, answer):
  if game_win:
    print("😃😃😃 YOU WON THE GAME 😃😃😃")
  else: 
    print(f"The correct answer was: {answer}")


def main():
  answer = generate_number()
  game_win = False
  attempts = 5
  guesses = []
  guess_state = None

  while not game_win and attempts > 0:
    try: 
      print(answer)
      guess = int(input("Guess a number between 1-100: "))
      game_win = check_guess(guess, guesses, answer)
      guesses.append(guess)
      attempts -= 1
    except:
      print("C'mon man, not a number!")
  
  end_game(game_win, answer)


main()